---
title: Mitmachen
slug: mitmachen
menu: main
weight: -2
description: Infos was ihr bei uns tun könnt
---
Meldet euch unter info@freisam.bayern damit wir euch in unsere Gruppe aufnehmen können.
Am besten ihr schaut euch vorher mal das Leitbild an damit wir auch gut zueinander passen.

Es gibt aktuell wöchentliche Treffen mit der Gesamtgruppe außerdem befinden sich einige Arbeitsgruppen (AGs)
in Gründung.

Falls ihr Fehler auf der Homepage findet meldet sie gerne mit einem Issue unter https://gitlab.com/arathornestel/freisam-hp aber ihr könnt natürlich auch einfach eine Mail schreiben. Vielen Dank schon mal! ;)
