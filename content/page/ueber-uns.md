---
title: Über uns
description: Kerngruppe für das Genossenschaftswohnprojekt im Steinpark - Freising
menu: main
slug: ueber-uns
draft: true
weight: -210
---

Größe: Derzeit ca. 25 aktive Mitglieder

Aktuell ist die Gruppe in der Orientierungsphase und gerade dabei, die gemeinsamen Ziele und Wünsche an das Wohnprojekt auszuformulieren und ein gemeinsames Leitbild zu erarbeiten.

Kontaktaufnahme und die Möglichkeit, sich auf die Interessentenliste eintragen zu lassen:

info@freisam.bayern
