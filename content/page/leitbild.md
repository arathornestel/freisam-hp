---
date: 2017-09-28T08:00:00+06:00
title: Leitbild
slug: leitbild
menu: main
weight: -230
---
#### Gemeinschaftlich - Gesellschaftlich - Finanziell - Baulich

Wir wünschen uns ein gemeinschaftsorientiertes Genossenschafts­projekt, in dem im Gegensatz zu zufälligen Nachbarschaften bewusst ein soziales Netz­werk aufgebaut wird. Nachbarschaft und Wohnanlage werden in Selbst­verwaltung von den Bewohnern organisiert und gepflegt.

Geplant ist gemeinsame Nutzung, Pflege und Eigentum von Dingen und Räumen: Co-Working-Spaces/ Gäste-Zimmer, Werkstatt, Arbeitsräume, Gemeinschaftsräume als Treffpunkte für Zusammenkünfte und zum Feiern, gemeinschaftlich nutzbare Dachterrassen, Garten, Grünflächen, Spielflächen, Ruhe-Zonen, Fahrrad-Werkstatt, Car-Sharing, Lastenräder, Mobilitätskonzept.

Für die Gebäude planen wir eine nachhaltige langlebige Bauweise: einfach bauen ohne übertriebene Technik mit hoher Aufenthaltsqualität. Flexible Grundrisse mit vorbereiteten Öffnungsmöglichkeiten (bei sinkenden, oder erhöhtem Wohnbedarf können Wohnungen angepasst oder getauscht werden). Das Baukonzept basiert Nachhaltigkeit, CO2-Ersparnis, Mobilitätskonzept, Inklusion, Car-Sharing, und vielen weiteren Faktoren. 

