---
title: Aktuell
menu:
  - main
weight: -270
---
> Nach der Zoom-Auftaktveranstaltung der Stadt Freising im Juli 2021 gab es eine erste Besichtigung des Geländes; mehrere Videokonferenzen und Präsenztreffen zum persönlichen Kennenlernen und zum Austausch über die Erwartungen an eine Genossenschaft und über die Wünsche an das gemein­same Leben in einer Genossenschaftswohnanlage.

Es bestehen erste Kontakte zu Architekten und Genossenschaften und Beratung durch die Mitbauzentrale. Dazu das Anmieten eines Raums für regelmäßige Treffen; und das Einrichten der Website.
>Größe: Derzeit ca. 25 aktive Mitglieder

Aktuell ist die Gruppe in der Orientierungsphase und gerade dabei, die gemeinsamen Ziele und Wünsche an das Wohnprojekt auszuformulieren und ein gemeinsames Leitbild zu erarbeiten.
> 
Noch mehr Test
